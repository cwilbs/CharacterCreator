# CharacterCreator
This project seeks to create a java program that will walk users through the character creation process for the 5th edition of Dungeons and Dragons RPG.

It is a continuation from an assignment that was turned in for the Fall 2017, CS495 capstone class at the University of Maryland University College. The original class GitHub repo can be found here: https://github.com/Ian-RSager/CMSC495

Initial contributers / team members included: 
* Anna Kane 
* Alexander LaBrie
* Brian McGillen
* Ian Sager
* Chris Wilbur

Contributers as of Jan 2018:
* Alexander LaBrie
* Ian Sager
* Chris Wilbur

NOTE: Images were pulled from general/open internet resources and will not be used for financial gains.
If this infringes upon any copyright rules or if the owners of the image(s) require removal of their image(s), please let us know and the images will be removed.

_**Change Log**_
----------

*2018-01-06*
----------
* Overhauled classtypes.json - file no longer exists, has been split out into individual files for each character class.  New CharacterClasses folder created in CharacterCreator/DataFiles, to hold these new files. (AL)
* Overhauled each individual class's formatting - including addition of a *level* and *subclassChoice* field, as well as alterations to the way proficiencies and choices are encoded.  Should allow for modular design.  These alterations currently only exist in the json files, ClassType.java has *not* been updated. (AL)
* Added *proficiency* field to each weapon in weapons.json, currently this is merely an exact copy of the *id*, will be necessary however for magical weapons/weapons with different names. (AL)
* Altered armor.json to reduce unnecessary verbosity in naming. (AL)

----------
*2018-01-14*
----------
Changes made to CharacterCreator.java & NewCharacter.java
 * Made the fields "private" to improve encapsulation:
 * Moved the fields more localized to improve encapsulation:
 * Set window to fill entire screen
 * Added to commenting to improve readability
 * FileChooser now loads/saves to CharacterFiles folder
 * All images now load from Images folder
 * Implemented Lambda ActionListeners
