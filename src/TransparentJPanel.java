import javax.swing.*;

class TransparentJPanel extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	{
		setOpaque(false);
		setFocusable(true);
	}
}
