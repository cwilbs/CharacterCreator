import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;
import java.awt.*;

class StatsPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    public JLabel statLabel;
    public SpinnerModel model;
    public int value = 0;
    JLabel statNameLabel;
    JLabel statNumberLabel;
    String inStat;
    JLabel maxHitPointLabel;

    public StatsPanel(String inStat, Character inCharacter, CharacterCreator cc)

    {
        this.inStat = inStat;
        setOpaque(false);
        Border margin = new EmptyBorder(5, 5, 0, 5);
        Border padding = new EmptyBorder(5, 5, 5, 5);
        Border border = BorderFactory.createLineBorder(Color.BLACK, 1, true);
        Border inside = new CompoundBorder(border, padding);
        setBorder(new CompoundBorder(margin, inside));
        setLayout(new BorderLayout());
        statNameLabel = new JLabel(inStat, SwingConstants.CENTER);
        add(statNameLabel, BorderLayout.NORTH);
        switch (inStat) {
            case "Hit Points":
                value = inCharacter.getHitPointsCurrent();
                break;
            case "Armor Class":
                value = inCharacter.getArmorClass();
                break;
            case "Initiative":
                value = inCharacter.getInitiative();
                break;
            case "Proficiency Bonus":
                value = inCharacter.getProficiencyBonus();
                break;
            case "Speed":
                value = inCharacter.getSpeed();
                break;
            case "Passive Perception":
                value = inCharacter.getPassivePerception();
                break;
        }

        if (inStat.matches("Proficiency Bonus") || inStat.matches("Passive Perception")
                || inStat.matches("Initiative")) {
            statLabel = new JLabel(String.valueOf(value));
            statLabel.setFont(new Font(null, Font.BOLD, 20));
            statLabel.setHorizontalAlignment(JLabel.CENTER);
            add(statLabel, BorderLayout.CENTER);
        } else {
            model = new SpinnerNumberModel(value, 0, 99, 1);
            JSpinner spinner = new JSpinner(model);
            spinner.setFont(new Font(null, Font.BOLD, 20));
            spinner.setOpaque(false);
            spinner.getEditor().setOpaque(false);
            JFormattedTextField textField = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
            textField.setOpaque(false);
            textField.setColumns(2);
            textField.setBorder(new EmptyBorder(0, 0, 0, 5));
            textField.setEditable(false);
            JPanel helperPanel = new TransparentJPanel();
            helperPanel.add(spinner);
            if (inStat.matches("Hit Points")) {
                maxHitPointLabel = new JLabel("/" + inCharacter.getHitPointsMaximum());
                maxHitPointLabel.setOpaque(false);
                helperPanel.add(maxHitPointLabel);
            }
            add(helperPanel, BorderLayout.CENTER);

            // listener to change set new values in character.java if spinner buttons are
            // used
            DefaultFormatter formatter = (DefaultFormatter) textField.getFormatter();
            formatter.setCommitsOnValidEdit(true);
            spinner.addChangeListener(new ChangeListener() {

                @Override
                public void stateChanged(ChangeEvent e) {
                    switch (inStat) {
                        case "Hit Points":
                            inCharacter.setHitPointsCurrent((int) spinner.getValue());
                            break;
                        case "Armor Class":
                            inCharacter.setArmorClass((int) spinner.getValue());
                            break;
                        case "Speed":
                            inCharacter.setSpeed((int) spinner.getValue());
                            break;
                        default:
                            break;
                    }
                    cc.refresh();

                }
            });
        }
    }
}
