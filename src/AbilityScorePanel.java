import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;
import java.awt.*;

class AbilityScorePanel extends JPanel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    JLabel abilityScoreNameLabel;
    JSpinner spinner;
    JLabel modifierLabel;
    int value = 1;
    int modifier = 0;

    public AbilityScorePanel(String inAbility, Character inCharacter, CharacterCreator cc) {

        setOpaque(false);
        Border margin = new EmptyBorder(5, 5, 0, 5);
        Border padding = new EmptyBorder(5, 5, 5, 5);
        Border border = BorderFactory.createBevelBorder(BevelBorder.RAISED);
        Border inside = new CompoundBorder(border, padding);
        setBorder(new CompoundBorder(margin, inside));
        setLayout(new BorderLayout());

        switch (inAbility) {
            case "Strength":
                value = inCharacter.getAbilityScoreStrength();
                break;
            case "Dexterity":
                value = inCharacter.getAbilityScoreDexterity();
                break;
            case "Constitution":
                value = inCharacter.getAbilityScoreConstitution();
                break;
            case "Intelligence":
                value = inCharacter.getAbilityScoreIntelligence();
                break;
            case "Wisdom":
                value = inCharacter.getAbilityScoreWisdom();
                break;
            case "Charisma":
                value = inCharacter.getAbilityScoreCharisma();
                break;
            default:
                break;
        }

        SpinnerModel model = new SpinnerNumberModel(value, 1, 30, 1);
        spinner = new JSpinner(model);
        spinner.setFont(new Font(null, Font.BOLD, 24));
        spinner.setOpaque(false);
        spinner.getEditor().setOpaque(false);
        JFormattedTextField textField = ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField();
        textField.setOpaque(false);
        textField.setColumns(2);
        textField.setBorder(new EmptyBorder(0, 0, 0, 5));
        textField.setEditable(false);

        // listener to change set new values in character.java if spinner buttons are
        // used
        DefaultFormatter formatter = (DefaultFormatter) textField.getFormatter();
        formatter.setCommitsOnValidEdit(true);
        spinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                switch (inAbility) {
                    case "Strength":
                        inCharacter.setAbilityScoreStrength((int) spinner.getValue());
                        break;
                    case "Dexterity":
                        inCharacter.setAbilityScoreDexterity((int) spinner.getValue());
                        break;
                    case "Constitution":
                        inCharacter.setAbilityScoreConstitution((int) spinner.getValue());
                        break;
                    case "Intelligence":
                        inCharacter.setAbilityScoreIntelligence((int) spinner.getValue());
                        break;
                    case "Wisdom":
                        inCharacter.setAbilityScoreWisdom((int) spinner.getValue());
                        break;
                    case "Charisma":
                        inCharacter.setAbilityScoreCharisma((int) spinner.getValue());
                        break;
                    default:
                        break;
                }
                modifier = AttributeBonusCalculator.getAttributeBonus((int) spinner.getValue());
                String modifierString = String.valueOf(modifier);
                if (modifier > 0)
                    modifierString = "+" + modifierString;
                modifierLabel.setText(" (" + modifierString + ")");
                cc.refresh();
            }
        });

        abilityScoreNameLabel = new JLabel(inAbility, SwingConstants.CENTER);
        modifier = AttributeBonusCalculator.getAttributeBonus(value);
        String modifierString = String.valueOf(modifier);
        if (modifier > 0)
            modifierString = "+" + modifierString;
        modifierLabel = new JLabel((" (" + modifierString + ")"), SwingConstants.RIGHT);
        modifierLabel.setFont(new Font(null, Font.ITALIC, 11));
        add(abilityScoreNameLabel, BorderLayout.NORTH);
        JPanel helperPanel = new TransparentJPanel();
        helperPanel.add(spinner);
        add(helperPanel, BorderLayout.CENTER);
        helperPanel.add(modifierLabel);
    }
}
