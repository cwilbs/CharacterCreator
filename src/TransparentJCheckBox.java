import javax.swing.*;

class TransparentJCheckBox extends JCheckBox {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public TransparentJCheckBox(String text, Boolean selected) {
		super(text, selected);
		setOpaque(false);
	}
}
