//package charactercreator;

/*
 * This class is used to define ClassType objects, more commonly
 * referred to simply as classes, or character classes.
 * These objects contain the necessary data and references
 * to generate a character class.
 */

public class ClassType extends Thing {

    private int[] hitDice;                        // An array determining which hit die the character uses.  There are 4 possible values, signifying: d6, d8, d10, d12.
    private String[] armorProficiencies;          // A list of the armor proficiencies the character gets.
    private String[] weaponProficiencies;         // A list of the weapon proficiencies the character gets.
    private String[] savingThrows;                // A list of the saving throws the character gains proficiency in.
    private int numberOfSkillChoices;             // A number signifying the quantity of choices the player is able to make from the skillChoicesArray, to gain proficiency in those skills.
    private String[] skillChoicesArray;           // A list of the skills the player may choose from, to gain proficiency in when creating a character of this class.
    private String[] tools;                       // A list of the tools the character gains proficiency in.
    private String equipment;                     // A description of the equipment the player may choose when creating a new character of this class.  For front-end use only.
    private String[][] features;                  // A list of the features the class gains at each level.  Each feature is listed via its unique ID, per the ClassFeature class.
    private String[] subClassChoices;             // A list of the possible subclasses the player may choose from, when creating a character with levels in this class.
    private String[][] subClassFeaturesList;      // A list of all the class features each of the possible subclasses receives.  21 entries in total, the first to be used as an identifier, and indices 1-20 signifying which feature is received at levels 1 through 20 for this particular subclass.

    // Constructor to be used when reading in data from classTypes.json
    public ClassType(String idIn, String nameIn, String descriptionIn, int[] hitDiceIn,
                     String[] armorProficienciesIn, String[] weaponsProficienciesIn, String[] savingThrowsIn,
                     int numberOfSkillChoicesIn, String[] skillChoicesArrayIn, String[] toolsIn, String equipmentIn,
                     String[][] featuresIn, String[][] subClassFeaturesListIn) {
        super(idIn, nameIn, descriptionIn);
        this.hitDice = hitDiceIn;
        this.armorProficiencies = armorProficienciesIn;
        this.weaponProficiencies = weaponsProficienciesIn;
        this.savingThrows = savingThrowsIn;
        this.numberOfSkillChoices = numberOfSkillChoicesIn;
        this.skillChoicesArray = skillChoicesArrayIn;
        this.tools = toolsIn;
        this.equipment = equipmentIn;
        this.features = featuresIn;
        this.subClassFeaturesList = subClassFeaturesListIn;
    }// end constructor

    // Only getter methods will be used, as these values should not change during the operation of the program.
    public int[] getHitDice() {
        return this.hitDice;
    }// end method

    public String[] getArmorProficiencies() {
        return this.armorProficiencies;
    }// end method

    public String[] getWeaponProficiencies() {
        return this.weaponProficiencies;
    }// end method

    public String[] getSavingThrows() {
        return this.savingThrows;
    }// end method

    public int getNumberOfSkillChoices() {
        return this.numberOfSkillChoices;
    }// end method

    public String[] getSkillChoicesArray() {
        return this.skillChoicesArray;
    }// end method

    public String[] getTools() {
        return this.tools;
    }// end method

    public String getEquipment() {
        return this.equipment;
    }// end method

    public String[][] getFeatures() {
        return this.features;
    }// end method

    public String[] getSubClassChoices() {
        return this.subClassChoices;
    }// end method

    public String[][] getSubClassFeaturesList() {
        return this.subClassFeaturesList;
    }// end method

} // End public class ClassType.
