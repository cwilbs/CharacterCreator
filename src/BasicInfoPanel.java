import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

class BasicInfoPanel extends JPanel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    JLabel basicInfoNameLabel;
    JTextField basicInfoValue;
    String value;
    Character character;

    public BasicInfoPanel(String inName, String inValue, Character inCharacter) {
        this.value = inValue;
        this.character = inCharacter;
        setOpaque(false);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        basicInfoValue = new JTextField(inValue);
        basicInfoValue.setOpaque(false);
        basicInfoValue.setEditable(false);
        basicInfoValue.setHorizontalAlignment(JTextField.CENTER);
        basicInfoValue.setFont(new Font(null, Font.BOLD, 20));
        basicInfoValue.setBackground(new Color(0, 0, 0, 0));
        basicInfoValue.setBorder(new EmptyBorder(0, 0, 0, 0));
        add(basicInfoValue);
        basicInfoNameLabel = new JLabel(inName);
        basicInfoNameLabel.setOpaque(false);
        basicInfoNameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        basicInfoNameLabel.setBorder(new EmptyBorder(0, 0, 8, 0));
        add(basicInfoNameLabel);
    }

    public void setEditable(JTextField basicInfoValueIn) {
        basicInfoValue.setEditable(true);
        basicInfoValue.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                basicInfoValue.setBackground(new Color(255, 255, 255));
                basicInfoValue.setOpaque(true);
                basicInfoValue.repaint();

            }

            @Override
            public void focusLost(FocusEvent e) {
                basicInfoValue.setBackground(new Color(0, 0, 0));
                basicInfoValue.setOpaque(false);
                basicInfoValue.repaint();
            }
        });
    }

    public void addSpinner() {
        JSpinner basicInfoValueSpinner;
        int spinnerValue = Integer.valueOf(value);
        remove(basicInfoValue);
        SpinnerModel model = new SpinnerNumberModel(spinnerValue, 0, 9999, 1);
        basicInfoValueSpinner = new JSpinner(model);
        basicInfoValueSpinner.setFont(new Font(null, Font.BOLD, 24));
        basicInfoValueSpinner.setOpaque(false);
        basicInfoValueSpinner.getEditor().setOpaque(false);
        JFormattedTextField textField = ((JSpinner.DefaultEditor) basicInfoValueSpinner.getEditor()).getTextField();
        textField.setOpaque(false);
        textField.setColumns(2);
        textField.setBorder(new EmptyBorder(0, 0, 0, 5));
        textField.setEditable(false);
        JPanel helperPanel = new TransparentJPanel();
        helperPanel.add(basicInfoValueSpinner);
        add(helperPanel, BorderLayout.CENTER);
        add(helperPanel);
        add(basicInfoNameLabel);

        DefaultFormatter formatter = (DefaultFormatter) textField.getFormatter();
        formatter.setCommitsOnValidEdit(true);
        basicInfoValueSpinner.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                character.setExperiencePoints((int) basicInfoValueSpinner.getValue());

            }
        });
    }
}
